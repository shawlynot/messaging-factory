package com.ph.coding.messaging.memory;

import com.ph.coding.messaging.MessageReceiver;
import com.ph.coding.messaging.MessageSender;
import com.ph.coding.messaging.MessagingException;
import com.ph.coding.messaging.MessagingFactory;
import com.ph.coding.messaging.memory.components.InMemoryMessageBroker;
import com.ph.coding.messaging.memory.components.InMemoryMessageReceiver;
import com.ph.coding.messaging.memory.components.InMemoryMessageSender;

import java.util.List;

/**
 * createSender an createReceiver should be threadSafe due to the use of CHM and the COWArrayList
 */
public class MemoryMessagingFactory implements MessagingFactory {

    private final InMemoryMessageBroker broker;

    public MemoryMessagingFactory() {
        this.broker = new InMemoryMessageBroker();
    }

    @Override
    public String getProviderName() {
        return "InMemory";
    }

    @Override
    public void shutdown() throws Exception {
        broker.shutDown();
    }

    @Override
    public MessageSender createSender(final String topic) throws MessagingException {
        validateTopic(topic);
        broker.createOrGetTopic(topic);
        return new InMemoryMessageSender(topic, broker);
    }

    @Override
    public MessageReceiver createReceiver(final String topic) throws MessagingException {
        validateTopic(topic);
        List<InMemoryMessageReceiver> messageReceivers = broker.createOrGetTopic(topic);
        InMemoryMessageReceiver receiver = new InMemoryMessageReceiver(topic, () -> {});
        messageReceivers.add(receiver);
        return receiver;
    }

    private void validateTopic(String topic) throws MessagingException {
        if (topic == null){
            throw new MessagingException("Topic cannot be null");
        }
    }

    /**
     * Method which can be called by unit test code to wait for all messages to be delivered and processed. This should cater
     * for message receivers which themselves send further messages, i.e. also waiting for such subsequent messages to be consumed.
     *
     * @param timeoutMillis Overall timeout to wait for the messaging infrastructure to become idle.
     * @throws Exception If something goes wrong whilst waiting, or messages are still waiting to or being handled when the
     *                   timeout expires.
     */
    public void waitForMessages(final long timeoutMillis) throws Exception {
        broker.waitUntilQueuesEmpty(timeoutMillis);
    }
}
