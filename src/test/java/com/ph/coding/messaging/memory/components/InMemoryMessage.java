package com.ph.coding.messaging.memory.components;

import com.ph.coding.messaging.Message;

import java.util.Arrays;

public class InMemoryMessage implements Message {

    private final byte[] msg;
    private final String topic;

    public InMemoryMessage(byte[] msg, String topic) {
        this.msg = msg;
        this.topic = topic;
    }

    static InMemoryMessage copy(InMemoryMessage message){
        return new InMemoryMessage(Arrays.copyOf(message.msg, message.msg.length), message.topic);
    }

    @Override
    public byte[] getMsg() {
        return msg;
    }

    @Override
    public void dispose() {
        System.out.println("Disposing of msg " + new String(msg) + "on topic " + topic);
    }
}
