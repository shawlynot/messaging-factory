package com.ph.coding.messaging.memory.components;

import com.ph.coding.messaging.*;

public class InMemoryMessageSender implements MessageSender {

    private final String topic;
    private final InMemoryMessageBroker broker;

    public InMemoryMessageSender(String topic, InMemoryMessageBroker broker) {
        this.topic = topic;
        this.broker = broker;
    }

    @Override
    public String getTopic() {
        return topic;
    }

    @Override
    public void sendMessage(byte[] message) throws MessagingException {
        broker.sendMessage(message, topic);
    }
}
