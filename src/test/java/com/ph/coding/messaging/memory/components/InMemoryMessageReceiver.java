package com.ph.coding.messaging.memory.components;

import com.ph.coding.messaging.Message;
import com.ph.coding.messaging.MessageReceiveListener;
import com.ph.coding.messaging.MessageReceiver;
import com.ph.coding.messaging.MessagingException;

public class InMemoryMessageReceiver implements MessageReceiver {

    private final String topic;
    private final Runnable onMessageProcessed;
    private MessageReceiveListener listener;

    public InMemoryMessageReceiver(String topic, Runnable decrementProcessedMessages) {
        this.topic = topic;
        this.onMessageProcessed = decrementProcessedMessages;
    }

    @Override
    public String getTopic() {
        return topic;
    }

    @Override
    public void setListener(MessageReceiveListener listener) {
        if (this.listener != null) {
            throw new RuntimeException(new MessagingException("Cannot reassign listener"));
        }
        if (listener == null) {
            throw new RuntimeException(new MessagingException("Listener cannot be null"));
        }
        this.listener = listener;
    }

    void processMessage(Message message) {
        // In the test case, it appears the StoringListener is not thread safe, but the same instance is
        // used from multiple threads. I am assuming therefore for this exercise the
        // MessageReceiverListeners must be called in a thread safe manner.
        try {
            synchronized (listener) {
                listener.onMessage(message, topic);
            }
        } finally {
            onMessageProcessed.run();
            message.dispose();
        }
    }
}
